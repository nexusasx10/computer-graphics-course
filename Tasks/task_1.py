from tkinter import Tk, Canvas
import math


def draw(canvas, width, height):

    a = 0
    b = math.pi + 1

    def f(x):
        return x * math.sin(x**2 / 2)

    if a > b:
        a, b = b, a
    elif a == b:
        return

    dx = b - a
    p = width / dx

    dots = []
    x_axis = None
    y_axis = None

    sub = a if a < 0 else 0

    for x in range(width):
        try:
            y = f(x / p + sub)
        except:
            continue
        dots.append((x, y))

    if a < 0 < b:
        y_axis = -sub * p

    max_y = max(map(lambda d: d[1], dots))
    min_y = min(map(lambda d: d[1], dots))

    dy = max_y - min_y
    if dy == 0:
        p = 0
    else:
        p = height / dy
    add = -min_y if min_y < 0 else 0

    if min_y < 0 < max_y:
        x_axis = height - add * p

    if x_axis is not None:
        canvas.create_line(0, x_axis, width, x_axis, fill='#aaa')
    if y_axis is not None:
        canvas.create_line(y_axis, 0, y_axis, height, fill='#aaa')

    for i in range(len(dots) - 1):
        canvas.create_line(
            math.floor(dots[i][0]),
            math.floor(height - (add + dots[i][1]) * p),
            math.floor(dots[i + 1][0]),
            math.floor(height - (add + dots[i + 1][1]) * p)
        )


def main():
    tk = Tk()
    width = 640
    height = 480
    tk.resizable(False, False)
    canvas = Canvas(
        width=width - 1,
        height=height + 1,
        bg='#ffffaa',
        highlightthickness=0
    )
    canvas.pack(fill='both', expand=True)
    draw(canvas, width, height)
    tk.mainloop()


if __name__ == '__main__':
    main()
