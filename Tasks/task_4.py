import math
from tkinter import Tk, Canvas


def draw(canvas, radius):
    def f(xp, yp):
        return math.cos(xp * yp)

    size = 2 * radius

    min_x = -2
    max_x = 1
    min_y = 1
    max_y = 4
    min_z = -3
    max_z = 3

    dx = max_x - min_x
    dy = max_y - min_y
    dz = max_z - min_z

    p = size / dx
    q = size / dy
    r = size / dz

    offset_x = radius * math.sqrt(3)
    offset_y = radius * 2

    density_x = 5
    density_y = 5

    # for x in range(2 * radius):
    #     pixel(canvas, *isometry(x + min_x, 0, 0), offset_x, offset_y, 'red')
    #
    # for y in range(2 * radius):
    #     pixel(canvas, *isometry(0, y + min_y, 0), offset_x, offset_y, 'green')
    #
    # for z in range(2 * radius):
    #     pixel(canvas, *isometry(0, 0, z + min_z), offset_x, offset_y, 'blue')

    up_horizon, down_horizon = init_horizons(4 * radius)
    for x in range(2 * radius, -1, -density_x):  # todo коэф.
        last = None
        for y in range(2 * radius, -1, -1):
            value = tuple(map(int, isometry(x, y, (f(x/p + min_x, y/q + min_y) - min_z) * r)))
            do = False
            color = None
            if value[1] > up_horizon[value[0]]:
                up_horizon[value[0]] = value[1]
                color = '#888'
                do = True
            if value[1] < down_horizon[value[0]]:
                down_horizon[value[0]] = value[1]
                color = 'black'
                do = True
            if not do:
                last = None
                continue
            if last:
                line(canvas, *value, *last, offset_x, offset_y, color)
            else:
                pixel(canvas, *value, offset_x, offset_y, color)
            last = value

    up_horizon, down_horizon = init_horizons(radius * 4)
    for y in range(2 * radius, -1, -density_y):
        last = None
        for x in range(2 * radius, -1, -1):
            value = tuple(map(int, isometry(x, y, (f(x/p + min_x, y/q + min_y) - min_z) * r)))
            do = False
            color = None
            if value[1] > up_horizon[value[0]]:
                up_horizon[value[0]] = value[1]
                color = '#888'
                do = True
            if value[1] < down_horizon[value[0]]:
                down_horizon[value[0]] = value[1]
                color = 'black'
                do = True
            if not do:
                last = None
                continue
            if last:
                line(canvas, *value, *last, offset_x, offset_y, color)
            else:
                pixel(canvas, *value, offset_x, offset_y, color)
            last = value


def init_horizons(size):
    return (
        [float('-inf') for _ in range(size)],
        [float('inf') for _ in range(size)]
    )


def isometry(x, y, z):
    xx = (y - x) * math.sqrt(3) / 2
    yy = (x + y) / 2 - z
    return xx, yy


def pixel(canvas, x, y, offset_x, offset_y, color):
    canvas.create_line(
        x + offset_x,
        y + offset_y,
        x + offset_x + 1,
        y + offset_y + 1,
        fill=color
    )


def line(canvas, x_start, y_start, x_end, y_end, offset_x, offset_y, color):
    canvas.create_line(
        x_start + offset_x,
        y_start + offset_y,
        x_end + offset_x,
        y_end + offset_y,
        fill=color
    )


def main():
    tk = Tk()
    radius = 150
    tk.resizable(False, False)
    tk.title('3D')
    canvas = Canvas(
        width=2 * radius * math.sqrt(3),
        height=4 * radius,
        bg='#ffffaa',
        highlightthickness=0
    )
    canvas.pack(fill='both', expand=True)
    draw(canvas, radius)
    tk.mainloop()


if __name__ == '__main__':
    main()
