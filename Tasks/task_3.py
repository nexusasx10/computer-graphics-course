from random import randint
from tkinter import Tk, Canvas
from PIL import Image, ImageDraw


#poly = [(100, 100), (100, 200), (200, 100), (200, 200)]
poly = []

taken = None
width = 480
height = 480


def press(event):
    if event.num == 1:
        if not add(event):
            take(event)
    elif event.num == 3:
        remove()


def release(event):
    if event.num == 1:
        drop()


def move(event):
    global taken
    if taken is not None:
        poly[taken] = event.x, event.y


def add(event):
    do = True
    for vertex in poly:
        if ((vertex[0] - event.x)**2 + (vertex[1] - event.y)**2) ** (1/2) < 3:
            do = False
            break
    if do:
        poly.append((event.x, event.y))
    return do


def remove():
    if poly:
        poly.pop()


def take(event):
    global taken
    for i, vertex in enumerate(poly):
        if ((vertex[0] - event.x) ** 2 + (vertex[1] - event.y) ** 2) ** (1 / 2) < 5:
            taken = i
            break


def drop():
    global taken
    taken = None


def paint(event):
    if len(poly) < 3:
        return
    image = Image.new('RGB', (width, height), 'white')
    drawer = ImageDraw.Draw(image)
    last = None
    drawer.line(poly[-1] + poly[0], (0, 0, 0))
    for vertex in poly:
        if last:
            drawer.line(last + vertex, (0, 0, 0))
        last = vertex
    visited = set()
    for x in range(width):
        for y in range(height):
            if (x, y) in visited:
                continue
            if image.getpixel((x, y)) == (0, 0, 0):
                continue
            if in_poly(x, y):
                fill(x, y, visited, image, rand_color())
            else:
                fill(x, y, visited, image, (255, 255, 254))
    image.show()


def rand_color():
    return randint(63, 192), randint(63, 192), randint(63, 192)


def fill(x, y, visited, image, color):
    stack = [(x, y)]
    while stack:
        point = stack.pop()
        visited.add(point)
        image.putpixel(point, color)
        for (ox, oy) in ((0, 1), (0, -1), (1, 0), (-1, 0)):
            nx, ny = point[0] + ox, point[1] + oy
            if not (0 <= nx < width and 0 <= ny < height):
                continue
            pixel = image.getpixel((nx, ny))
            if pixel != color and pixel != (0, 0, 0):
                stack.append((nx, ny))


def linear(y, x1, y1, x2, y2):
    if y2 == y1:
        return y1
    return y * (x2 - x1) / (y2 - y1) - (x1 * y2 - x2 * y1) / (y1 - y2)


def in_poly(x, y):
    last = None
    intersections = 0
    for i, vertex in enumerate(poly + [poly[0]]):
        if last:
            nx = linear(y, *last, *vertex)
            if x < nx and (last[1] < y < vertex[1] or vertex[1] < y < last[1]):
                intersections += 1
            elif vertex[1] == y or last[1] == y:
                next_ = vertex[1]
                prev = vertex[1]
                j = i
                k = i
                while prev == y:
                    j -= 1
                    if j == -1:
                        return True
                    prev = poly[j][1]
                while next_ == y:
                    k += 1
                    if k == len(poly):
                        return True
                    next_ = poly[k][1]
                if next_ > y > prev or prev > y > next_:
                    intersections += 1
        last = vertex
    return intersections % 2 == 1


def draw(tk, canvas):
    canvas.delete('all')
    last = None
    if len(poly) >= 2:
        canvas.create_line(*poly[-1], *poly[0])
    for vertex in poly:
        if last:
            canvas.create_line(*last, *vertex)
        last = vertex
    for vertex in poly:
        canvas.create_oval(
            vertex[0] - 3,
            vertex[1] - 3,
            vertex[0] + 3,
            vertex[1] + 3,
            fill='black'
        )
    if poly:
        canvas.create_oval(
            poly[-1][0] - 3,
            poly[-1][1] - 3,
            poly[-1][0] + 3,
            poly[-1][1] + 3,
            fill='green'
        )
        canvas.create_oval(
            poly[0][0] - 3,
            poly[0][1] - 3,
            poly[0][0] + 3,
            poly[0][1] + 3,
            fill='red'
        )
    tk.after(100, draw, tk, canvas)


def main():
    tk = Tk()
    tk.resizable(False, False)
    canvas = Canvas(
        width=width,
        height=height,
        bg='#ffffff',
        highlightthickness=0
    )
    canvas.pack(fill='both', expand=True)
    tk.bind('<ButtonPress>', press)
    tk.bind('<ButtonRelease>', release)
    tk.bind('<Motion>', move)
    tk.bind('<Return>', paint)
    tk.after(10, draw, tk, canvas)
    tk.mainloop()


if __name__ == '__main__':
    main()
