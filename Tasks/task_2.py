import math
from tkinter import Tk, Canvas


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance(self, other):
        return math.sqrt((self.x - other.x) ** 2 + (self.y - other.y) ** 2)


class Parabola:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c
        self.p = -b**2 / a
        self.directrix = -self.p / 2 + c + 1
        self.focus = Point(self.p / 2 + c + 1, 0)
        self.vertex = Point(c + 1, 0)
        self.direction = -1 if (-self.a / self.b**2) < 0 else 1

    def f(self, yp):
        return (-self.a / self.b**2) * yp**2 + self.c + 1


def draw(canvas, width, height, parabola: Parabola):
    left = -width // 2
    right = width // 2
    top = height // 2
    bottom = -height // 2

    offset_x = width // 2
    offset_y = height // 2

    for x in range(width):
        set_pixel(canvas, x, offset_y, '#aaa')

    for y in range(height):
        set_pixel(canvas, offset_x, y, '#aaa')

    x = parabola.vertex.x
    y = parabola.vertex.y
    set_pixel(canvas, x + offset_x, y + offset_y, 'black')
    while True:
        if parabola.direction == -1 and x < left:
            return
        if parabola.direction == 1 and x > right:
            return
        if not (bottom < y < top):
            return
        d = abs(abs(x + parabola.direction - parabola.directrix) - Point(x + parabola.direction, y + 1).distance(parabola.focus))
        v = abs(abs(x + parabola.direction - parabola.directrix) - Point(x + parabola.direction, y).distance(parabola.focus))
        h = abs(abs(x - parabola.directrix) - Point(x, y + 1).distance(parabola.focus))
        if abs(h) <= abs(v):
            if abs(d) < abs(h):
                x += parabola.direction
            y += 1
        else:
            if abs(v) > abs(d):
                y += 1
            x += parabola.direction
        set_pixel(canvas, x + offset_x, y + offset_y, 'black')
        set_pixel(canvas, x + offset_x, -y + offset_y, 'black')


def set_pixel(canvas, x, y, color):
    canvas.create_line(x, y, x + 1, y + 1, fill=color)


def main():
    tk = Tk()
    width = 640
    height = 640
    tk.resizable(False, False)
    tk.title('Bresenham')
    canvas = Canvas(
        width=width,
        height=height,
        bg='#ffffaa',
        highlightthickness=0
    )
    canvas.pack(fill='both', expand=True)
    draw(canvas, width, height, Parabola(-10, 20, 50))
    tk.mainloop()


if __name__ == '__main__':
    main()
